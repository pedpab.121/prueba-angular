import { Component, OnInit } from '@angular/core';

import listaDePersonas from 'src/assets/data.json';


@Component({
  selector: 'app-prueba',
  templateUrl: './prueba.component.html',
  styleUrls: ['./prueba.component.css']
})
export class PruebaComponent implements OnInit {

  Personas: any = listaDePersonas;

  constructor() {
    this.getJsonContent();
  }

  getJsonContent() {
    for (let i in listaDePersonas) {
      console.log(listaDePersonas[i].email)
    }
  }

  ngOnInit(): void {}

}
